use core::{
    convert::TryInto,
    fmt,
    mem,
    sync::atomic::{AtomicUsize, Ordering},
};

use lazy_static::lazy_static;
use volatile::Volatile;
use x86_64::{
    instructions::{interrupts, tables},
    registers::control::Cr2,
    structures::{
        idt::{Entry, EntryOptions},
        DescriptorTablePointer,
    },
    VirtAddr,
};

use ku::{memory::PageFaultInfo, process::Info, sync::spinlock::Spinlock};

use crate::{
    fs::BlockCache,
    log::{error, info, trace},
    memory::{size, Virt, DOUBLE_FAULT_IST_INDEX, PAGE_FAULT_IST_INDEX},
    process::{ModeContext, Pid, Process, Table},
    smp::{Cpu, LocalApic},
    time::{pit8254, rtc},
};


/// [Exceptions: Division Error](https://wiki.osdev.org/Exception#Division_Error)
pub const DIVIDE_ERROR: usize = 0x00;

/// [Exceptions: Debug](https://wiki.osdev.org/Exception#Debug)
pub const DEBUG: usize = 0x01;

/// [Non-maskable interrupt](https://en.wikipedia.org/wiki/Non-maskable_interrupt),
/// [Exceptions: Non-maskable interrupt](https://wiki.osdev.org/Non_Maskable_Interrupt)
pub const NON_MASKABLE_INTERRUPT: usize = 0x02;

/// [Exceptions: Breakpoint](https://wiki.osdev.org/Exception#Breakpoint)
pub const BREAKPOINT: usize = 0x03;

/// [Exceptions: Overflow](https://wiki.osdev.org/Exception#Overflow)
pub const OVERFLOW: usize = 0x04;

/// [Exceptions: Bound Range Exceeded](https://wiki.osdev.org/Exception#Bound_Range_Exceeded)
pub const BOUND_RANGE_EXCEEDED: usize = 0x05;

/// [Exceptions: Invalid Opcode](https://wiki.osdev.org/Exception#Invalid_Opcode)
pub const INVALID_OPCODE: usize = 0x06;

/// [Exceptions: Device Not Available](https://wiki.osdev.org/Exception#Device_Not_Available)
pub const DEVICE_NOT_AVAILABLE: usize = 0x07;

/// [Double Fault](https://en.wikipedia.org/wiki/Double_fault),
/// [Exceptions: Double Fault](https://wiki.osdev.org/Exception#Double_Fault)
pub const DOUBLE_FAULT: usize = 0x08;

/// [Exceptions: Invalid TSS](https://wiki.osdev.org/Exception#Invalid_TSS)
pub const INVALID_TSS: usize = 0x0A;

/// [Exceptions: Segment Not Present](https://wiki.osdev.org/Exception#Segment_Not_Present)
pub const SEGMENT_NOT_PRESENT: usize = 0x0B;

/// [Exceptions: Stack-Segment Fault](https://wiki.osdev.org/Exception#Stack-Segment_Fault)
pub const STACK_SEGMENT_FAULT: usize = 0x0C;

/// [General Protection Fault](https://en.wikipedia.org/wiki/General_protection_fault),
/// [Exceptions: General Protection Fault](https://wiki.osdev.org/Exception#General_Protection_Fault)
pub const GENERAL_PROTECTION_FAULT: usize = 0x0D;

/// [Page Fault](https://en.wikipedia.org/wiki/Page_fault),
/// [Exceptions: Page Fault](https://wiki.osdev.org/Exception#Page_Fault)
pub const PAGE_FAULT: usize = 0x0E;

/// [Exceptions: x87 Floating-Point Exception](https://wiki.osdev.org/Exception#x87_Floating-Point_Exception)
pub const X87_FLOATING_POINT: usize = 0x10;

/// [Exceptions: Alignment Check](https://wiki.osdev.org/Exception#Alignment_Check)
pub const ALIGNMENT_CHECK: usize = 0x11;

/// [Machine-check exception](https://en.wikipedia.org/wiki/Machine-check_exception),
/// [Exceptions: Machine Check](https://wiki.osdev.org/Exception#Machine_Check)
pub const MACHINE_CHECK: usize = 0x12;

/// [Exceptions: SIMD Floating-Point Exception](https://wiki.osdev.org/Exception#SIMD_Floating-Point_Exception)
pub const SIMD_FLOATING_POINT: usize = 0x13;

/// [Exceptions: Security Exception](https://wiki.osdev.org/Exception#Security_Exception)
pub const VIRTUALIZATION: usize = 0x14;

/// [Exceptions: Security Exception](https://wiki.osdev.org/Exception#Security_Exception)
pub const SECURITY_EXCEPTION: usize = 0x1E;


/// Количество
/// [исключений процессора](https://en.wikipedia.org/wiki/Interrupt_descriptor_table#Processor-generated_exceptions).
const EXCEPTION_COUNT: u8 = 0x20;

/// Первое прерывание
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259).
/// [Стандартная последовательность](https://wiki.osdev.org/Interrupts#Standard_ISA_IRQs)
/// подключения
/// [прерываний в x86](https://en.wikipedia.org/wiki/Interrupt_request_(PC_architecture))
/// для устройств шины
/// [Industry Standard Architecture](https://en.wikipedia.org/wiki/Industry_Standard_Architecture)
/// (ISA).
const PIC_FIRST_INTERRUPT: u8 = EXCEPTION_COUNT;

/// Первое прерывание
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259).
/// [Стандартная последовательность](https://wiki.osdev.org/Interrupts#Standard_ISA_IRQs)
/// подключения
/// [прерываний в x86](https://en.wikipedia.org/wiki/Interrupt_request_(PC_architecture))
/// для устройств шины
/// [Industry Standard Architecture](https://en.wikipedia.org/wiki/Industry_Standard_Architecture)
/// (ISA).
const PIC_BASE: usize = PIC_FIRST_INTERRUPT as usize;

/// Номер прерывания таймера [Intel 8253/8254](https://en.wikipedia.org/wiki/Intel_8253)
/// ([programmable interval timer, PIT](https://en.wikipedia.org/wiki/Programmable_interval_timer)).
pub const TIMER: usize = PIC_BASE;

/// Номер прерывания клавиатуры.
pub const KEYBOARD: usize = PIC_BASE + 0x1;

/// Номер входа первого контроллера
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259),
/// к которому каскадно подключён второй такой же.
pub const CASCADE: usize = PIC_BASE + 0x2;

/// Номер прерывания
/// [последовательных портов](https://en.wikipedia.org/wiki/Serial_port) номер 2 и 4.
pub const COM2: usize = PIC_BASE + 0x3;

/// Номер прерывания
/// [последовательных портов](https://en.wikipedia.org/wiki/Serial_port) номер 1 и 3.
pub const COM1: usize = PIC_BASE + 0x4;

/// [Номер прерывания](https://en.wikipedia.org/wiki/Parallel_port#Port_addresses)
/// второго параллельного порта
/// ([Parallel port](https://en.wikipedia.org/wiki/Parallel_port)).
/// Так как через параллельные порты чаще всего подключались принтеры
/// ([Line printer](https://en.wikipedia.org/wiki/Line_printer)),
/// сохранилось их сокращение LPT.
pub const LPT2: usize = PIC_BASE + 0x5;

/// Номер прерывания контроллера [дискет](https://en.wikipedia.org/wiki/Floppy_disk).
pub const FLOPPY_DISK: usize = PIC_BASE + 0x6;

/// [Номер прерывания](https://en.wikipedia.org/wiki/Parallel_port#Port_addresses)
/// первого и третьего параллельного порта
/// ([Parallel port](https://en.wikipedia.org/wiki/Parallel_port)).
/// Так как через параллельные порты чаще всего подключались принтеры
/// ([Line printer](https://en.wikipedia.org/wiki/Line_printer)),
/// сохранилось их сокращение LPT.
pub const LPT1: usize = PIC_BASE + 0x7;

/// Номер обработчика прерываний
/// [часов реального времени (Real-time clock, RTC)](https://en.wikipedia.org/wiki/Real-time_clock).
pub const RTC: usize = PIC_BASE + 0x8;

/// Номер прерывания незарезервированного входа `0x9` каскадной пары
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259).
pub const FREE_29: usize = PIC_BASE + 0x9;

/// Номер прерывания незарезервированного входа `0xA` каскадной пары
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259).
pub const FREE_2A: usize = PIC_BASE + 0xA;

/// Номер прерывания незарезервированного входа `0xB` каскадной пары
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259).
pub const FREE_2B: usize = PIC_BASE + 0xB;

/// Номер прерывания мыши.
pub const PS2_MOUSE: usize = PIC_BASE + 0xC;

/// Номер прерывания сопроцессора.
pub const COPROCESSOR: usize = PIC_BASE + 0xD;

/// Номер прерывания первого контроллера
/// [PATA](https://en.wikipedia.org/wiki/Parallel_ATA).
pub const ATA0: usize = PIC_BASE + 0xE;

/// Номер прерывания второго контроллера
/// [PATA](https://en.wikipedia.org/wiki/Parallel_ATA).
pub const ATA1: usize = PIC_BASE + 0xF;

/// Первое прерывание
/// [APIC](https://en.wikipedia.org/wiki/Advanced_Programmable_Interrupt_Controller#APIC_timer).
const APIC_BASE: usize = ATA1 + 1;

/// Номер прерывания
/// [таймера APIC](https://en.wikipedia.org/wiki/Advanced_Programmable_Interrupt_Controller#APIC_timer).
pub const APIC_TIMER: usize = APIC_BASE;

/// Номер ложных прерываний
/// ([spurious interrupt](https://en.wikipedia.org/wiki/Interrupt#Spurious_interrupts))
/// [APIC](https://en.wikipedia.org/wiki/Advanced_Programmable_Interrupt_Controller).
pub const APIC_SPURIOUS: usize = APIC_BASE + 0x1;

/// Количество исключений и прерываний.
const COUNT: usize = APIC_SPURIOUS + 1;


/// Информация о прерывании.
pub struct Statistics {
    /// Сколько раз сработало это прерывание.
    count: AtomicUsize,

    /// Короткая мнемоника прерывания.
    mnemonic: &'static str,

    /// Имя прерывания.
    name: &'static str,
}


impl Statistics {
    /// Создаёт информацию о прерывании с именем `name` и короткой мнемоникой `mnemonic`.
    const fn new(name: &'static str, mnemonic: &'static str) -> Statistics {
        Statistics {
            name,
            mnemonic,
            count: AtomicUsize::new(0),
        }
    }


    /// Сколько раз сработало это прерывание.
    pub fn count(&self) -> usize {
        self.count.load(Ordering::Relaxed)
    }


    /// Короткая мнемоника прерывания.
    pub fn mnemonic(&self) -> &'static str {
        self.mnemonic
    }


    /// Имя прерывания.
    pub fn name(&self) -> &'static str {
        self.name
    }


    /// Инкрементирует счётчик срабатывания прерывания.
    fn inc(&self) {
        self.count.fetch_add(1, Ordering::Relaxed);
    }
}


/// Информация обо всех прерываниях.
type InterruptStats = [Statistics; COUNT];


/// Информация обо всех прерываниях.
pub static INTERRUPT_STATS: InterruptStats = [
    Statistics::new("Divide Error", "#DE"),
    Statistics::new("Debug", "#DB"),
    Statistics::new("Non-maskable Interrupt", "#NM"),
    Statistics::new("Breakpoint", "#BP"),
    Statistics::new("Overflow", "#OF"),
    Statistics::new("Bound Range Exceeded", "#BR"),
    Statistics::new("Invalid Opcode", "#UD"),
    Statistics::new("Device Not Available", "#NA"),
    Statistics::new("Double Fault", "#DF"),
    Statistics::new("Coprocessor Segment Overrun", "#CS"),
    Statistics::new("Invalid TSS", "#TS"),
    Statistics::new("Segment Not Present", "#NP"),
    Statistics::new("Stack-Segment Fault", "#SS"),
    Statistics::new("General Protection Fault", "#GP"),
    Statistics::new("Page Fault", "#PF"),
    Statistics::new("Reserved 0x0F", "#0F"),
    Statistics::new("x87 Floating-Point Exception", "#MF"),
    Statistics::new("Alignment Check", "#AC"),
    Statistics::new("Machine Check", "#MC"),
    Statistics::new("SIMD Floating-Point Exception", "#XF"),
    Statistics::new("Virtualization Exception", "#VE"),
    Statistics::new("Reserved 0x15", "#15"),
    Statistics::new("Reserved 0x16", "#16"),
    Statistics::new("Reserved 0x17", "#17"),
    Statistics::new("Reserved 0x18", "#18"),
    Statistics::new("Reserved 0x19", "#19"),
    Statistics::new("Reserved 0x1A", "#1A"),
    Statistics::new("Reserved 0x1B", "#1B"),
    Statistics::new("Reserved 0x1C", "#1C"),
    Statistics::new("Reserved 0x1D", "#1D"),
    Statistics::new("Security Exception", "#SX"),
    Statistics::new("Reserved 0x1F", "#1F"),
    Statistics::new("Timer", "#TI"),
    Statistics::new("Keyboard", "#KB"),
    Statistics::new("Cascade", "#CA"),
    Statistics::new("COM2", "#C2"),
    Statistics::new("COM1", "#C1"),
    Statistics::new("LPT2", "#L2"),
    Statistics::new("Floppy Disk", "#FD"),
    Statistics::new("LPT1", "#L1"),
    Statistics::new("RTC", "#RT"),
    Statistics::new("Free 0x29", "#29"),
    Statistics::new("Free 0x2A", "#2A"),
    Statistics::new("Free 0x2B", "#2B"),
    Statistics::new("PS2 Mouse", "#MS"),
    Statistics::new("Coprocessor", "#CP"),
    Statistics::new("Primary ATA Hard Disk", "#PD"),
    Statistics::new("Secondary ATA Hard Disk", "#SD"),
    Statistics::new("APIC timer", "#AT"),
    Statistics::new("APIC spurious", "#AS"),
];


/// Инициализирует таблицу обработчиков прерываний [`struct@IDT`].
pub(super) fn init() {
    unsafe {
        pic8259::init(PIC_FIRST_INTERRUPT);
    }

    IDT.load();

    interrupts::enable();

    rtc::enable_next_interrupt();

    info!("interrupts init");
}


/// Таблица обработчиков прерываний
/// ([Interrupt descriptor table](https://en.wikipedia.org/wiki/Interrupt_descriptor_table), IDT).
pub(crate) struct Idt([IdtEntry; COUNT]);


impl Idt {
    /// Создаёт таблицу обработчиков прерываний
    /// ([Interrupt descriptor table](https://en.wikipedia.org/wiki/Interrupt_descriptor_table), IDT).
    fn new() -> Self {
        let mut idt = [IdtEntry::missing(); COUNT];

        unsafe {
            idt[DOUBLE_FAULT]
                .set_handler_with_error(double_fault)
                .set_stack_index(DOUBLE_FAULT_IST_INDEX);

            idt[PAGE_FAULT]
                .set_handler_with_error(page_fault)
                .set_stack_index(PAGE_FAULT_IST_INDEX);
        }

        idt[DIVIDE_ERROR].set_handler(divide_error);
        idt[DEBUG].set_handler(debug);
        idt[NON_MASKABLE_INTERRUPT].set_handler(non_maskable_interrupt);
        idt[BREAKPOINT].set_handler(breakpoint);
        idt[OVERFLOW].set_handler(overflow);
        idt[BOUND_RANGE_EXCEEDED].set_handler(bound_range_exceeded);
        idt[INVALID_OPCODE].set_handler(invalid_opcode);
        idt[DEVICE_NOT_AVAILABLE].set_handler(device_not_available);
        idt[INVALID_TSS].set_handler_with_error(invalid_tss);
        idt[SEGMENT_NOT_PRESENT].set_handler_with_error(segment_not_present);
        idt[STACK_SEGMENT_FAULT].set_handler_with_error(stack_segment_fault);
        idt[GENERAL_PROTECTION_FAULT].set_handler_with_error(general_protection_fault);
        idt[X87_FLOATING_POINT].set_handler(x87_floating_point);
        idt[ALIGNMENT_CHECK].set_handler_with_error(alignment_check);
        idt[MACHINE_CHECK].set_handler(machine_check);
        idt[SIMD_FLOATING_POINT].set_handler(simd_floating_point);
        idt[VIRTUALIZATION].set_handler(virtualization);
        idt[SECURITY_EXCEPTION].set_handler_with_error(security_exception);

        idt[TIMER].set_handler(timer);
        idt[KEYBOARD].set_handler(keyboard);
        idt[CASCADE].set_handler(cascade);
        idt[COM2].set_handler(com2);
        idt[COM1].set_handler(com1);
        idt[LPT2].set_handler(lpt2);
        idt[FLOPPY_DISK].set_handler(floppy_disk);
        idt[LPT1].set_handler(lpt1);
        idt[RTC].set_handler(rtc);
        idt[FREE_29].set_handler(free_29);
        idt[FREE_2A].set_handler(free_2a);
        idt[FREE_2B].set_handler(free_2b);
        idt[PS2_MOUSE].set_handler(ps2_mouse);
        idt[COPROCESSOR].set_handler(coprocessor);
        idt[ATA0].set_handler(ata0);
        idt[ATA1].set_handler(ata1);
        idt[APIC_TIMER].set_handler(apic_timer);
        idt[APIC_SPURIOUS].set_handler(apic_spurious);

        Self(idt)
    }


    /// Загружает дескриптор таблицы прерываний
    /// ([Interrupt descriptor table](https://en.wikipedia.org/wiki/Interrupt_descriptor_table), IDT)
    /// в регистр [`IDTR`](https://wiki.osdev.org/Interrupt_Descriptor_Table#IDTR).
    pub(crate) fn load(&self) {
        unsafe {
            let pseudo_descriptor = DescriptorTablePointer {
                base: Virt::from_ref(&self.0).into(),
                limit: (mem::size_of_val(&self.0) - 1).try_into().expect("the IDT is too large"),
            };
            tables::lidt(&pseudo_descriptor);
        }
    }
}


lazy_static! {
    /// Таблица обработчиков прерываний
    /// ([Interrupt descriptor table](https://en.wikipedia.org/wiki/Interrupt_descriptor_table), IDT).
    pub(crate) static ref IDT: Idt = Idt::new();
}


/// Обёртка для [`ModeContext`].
/// Она помечает запись в него как [`Volatile`], чтобы компилятор такую операцию записи не выкинул.
#[allow(rustdoc::private_intra_doc_links)]
#[repr(transparent)]
pub struct InterruptContext(ModeContext);


#[allow(rustdoc::private_intra_doc_links)]
impl InterruptContext {
    /// Возвращает `true`, если контекст имеет привилегии пользователя.
    pub fn is_user_mode(&self) -> bool {
        self.0.is_user_mode()
    }


    /// Возвращает [`ModeContext`], содержащийся в этом [`InterruptContext`].
    pub fn get(&self) -> ModeContext {
        self.0
    }


    /// Записывает `context` в этот [`InterruptContext`].
    /// Помечает запись как [`Volatile`], чтобы компилятор такую операцию записи не выкинул.
    pub fn set(&mut self, context: ModeContext) {
        Volatile::new(&mut self.0).write(context);
    }
}


impl fmt::Debug for InterruptContext {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{:?}", self.get())
    }
}


impl fmt::Display for InterruptContext {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", self.get())
    }
}


/// Тип функции обработчиков прерываний.
type InterruptHandler = fn(InterruptContext);


/// Запись таблицы прерываний
/// ([Interrupt descriptor table](https://en.wikipedia.org/wiki/Interrupt_descriptor_table), IDT).
#[derive(Clone, Copy)]
struct IdtEntry(Entry<InterruptHandler>);


impl IdtEntry {
    /// Создаёт запись таблицы прерываний без обработчика.
    const fn missing() -> Self {
        Self(Entry::missing())
    }


    /// Устанавливает обработчик `handler` исключения или прерывания.
    fn set_handler(
        &mut self,
        handler: extern "x86-interrupt" fn(InterruptContext),
    ) -> &mut EntryOptions {
        unsafe { self.0.set_handler_addr(VirtAddr::from_ptr(handler as *const ())) }
    }


    /// Устанавливает обработчик `handler` исключения,
    /// для которого процессор сохраняет дополнительный код ошибки.
    fn set_handler_with_error(
        &mut self,
        handler: extern "x86-interrupt" fn(InterruptContext, u64),
    ) -> &mut EntryOptions {
        unsafe { self.0.set_handler_addr(VirtAddr::from_ptr(handler as *const ())) }
    }
}


/// Фатально ли исключение.
#[derive(PartialEq)]
enum Fatal {
    /// Исключение фатально.
    Yes,

    /// Исключение не фатально.
    No,
}


/// Общий обработчик исключений и прерываний.
///
/// - `context` --- контекст в котором возникло прерывание.
/// - `number` --- номер прерывания.
/// - `info` --- дополнительная информация для некоторых исключений процессора.
/// - `fatal` --- фатально ли исключение.
/// Если фатальное исключение вызвал процесс, то он будет остановлен и удалён.
/// Если фатальное исключение вызвало ядро, оно запаникует.
fn generic_trap(context: &mut InterruptContext, number: usize, info: Info, fatal: Fatal) {
    trace!(trap = INTERRUPT_STATS[number].name, %context, %info);

    INTERRUPT_STATS[number].inc();

    if context.is_user_mode() {
        let pid = Cpu::current_process().expect("user mode without a process");
        if pid == Pid::Current {
            context.set(ModeContext::kernel_context(Virt::default()));
            return;
        }
        let mut process =
            Table::get(pid).expect("failed to find the current process in the process table");

        if process.trap(context, number, info) {
            return;
        }

        info!(
            trap = INTERRUPT_STATS[number].name,
            number,
            %info,
            %context,
            %pid,
            "user mode trap",
        );

        if fatal == Fatal::Yes {
            Table::free(process);
            Process::sched_yield();
        }
    } else {
        match BlockCache::trap_handler(&info) {
            Ok(true) => return,
            Ok(false) => {},
            Err(error) => error!(?error, "failed to handle a page fault in the block cache"),
        }

        if fatal == Fatal::Yes {
            panic!(
                "kernel mode trap #{:?} - {}, context: {}, info: {}",
                number, INTERRUPT_STATS[number].name, context, info,
            );
        } else {
            error!(
                trap = INTERRUPT_STATS[number].name,
                number,
                %info,
                %context,
                "kernel mode trap",
            );
        }
    }
}


/// [Exceptions: Division Error](https://wiki.osdev.org/Exception#Division_Error)
extern "x86-interrupt" fn divide_error(mut context: InterruptContext) {
    generic_trap(&mut context, DIVIDE_ERROR, Info::None, Fatal::Yes);
}


/// [Exceptions: Debug](https://wiki.osdev.org/Exception#Debug)
extern "x86-interrupt" fn debug(mut context: InterruptContext) {
    generic_trap(&mut context, DEBUG, Info::None, Fatal::Yes);
}


/// [Non-maskable interrupt](https://en.wikipedia.org/wiki/Non-maskable_interrupt),
/// [Exceptions: Non-maskable interrupt](https://wiki.osdev.org/Non_Maskable_Interrupt)
extern "x86-interrupt" fn non_maskable_interrupt(mut context: InterruptContext) {
    generic_trap(&mut context, NON_MASKABLE_INTERRUPT, Info::None, Fatal::Yes);
}


/// [Exceptions: Breakpoint](https://wiki.osdev.org/Exception#Breakpoint)
extern "x86-interrupt" fn breakpoint(mut context: InterruptContext) {
    generic_trap(&mut context, BREAKPOINT, Info::None, Fatal::No);
}


/// [Exceptions: Overflow](https://wiki.osdev.org/Exception#Overflow)
extern "x86-interrupt" fn overflow(mut context: InterruptContext) {
    generic_trap(&mut context, OVERFLOW, Info::None, Fatal::No);
}


/// [Exceptions: Bound Range Exceeded](https://wiki.osdev.org/Exception#Bound_Range_Exceeded)
extern "x86-interrupt" fn bound_range_exceeded(mut context: InterruptContext) {
    generic_trap(&mut context, BOUND_RANGE_EXCEEDED, Info::None, Fatal::Yes);
}


/// [Exceptions: Invalid Opcode](https://wiki.osdev.org/Exception#Invalid_Opcode)
extern "x86-interrupt" fn invalid_opcode(mut context: InterruptContext) {
    generic_trap(&mut context, INVALID_OPCODE, Info::None, Fatal::Yes);
}


/// [Exceptions: Device Not Available](https://wiki.osdev.org/Exception#Device_Not_Available)
extern "x86-interrupt" fn device_not_available(mut context: InterruptContext) {
    generic_trap(&mut context, DEVICE_NOT_AVAILABLE, Info::None, Fatal::Yes);
}


/// [Double Fault](https://en.wikipedia.org/wiki/Double_fault),
/// [Exceptions: Double Fault](https://wiki.osdev.org/Exception#Double_Fault)
extern "x86-interrupt" fn double_fault(mut context: InterruptContext, error_code: u64) {
    lazy_static! {
        static ref STOP_ALL_CPUS: Spinlock<()> = Spinlock::new(());
    }

    let _ = STOP_ALL_CPUS.lock();

    generic_trap(
        &mut context,
        DOUBLE_FAULT,
        Info::Code(error_code),
        Fatal::Yes,
    );
    unreachable!();
}


/// [Exceptions: Invalid TSS](https://wiki.osdev.org/Exception#Invalid_TSS)
extern "x86-interrupt" fn invalid_tss(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        INVALID_TSS,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


/// [Exceptions: Segment Not Present](https://wiki.osdev.org/Exception#Segment_Not_Present)
extern "x86-interrupt" fn segment_not_present(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        SEGMENT_NOT_PRESENT,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


/// [Exceptions: Stack-Segment Fault](https://wiki.osdev.org/Exception#Stack-Segment_Fault)
extern "x86-interrupt" fn stack_segment_fault(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        STACK_SEGMENT_FAULT,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


/// [General Protection Fault](https://en.wikipedia.org/wiki/General_protection_fault)
/// [Exceptions: General Protection Fault](https://wiki.osdev.org/Exception#General_Protection_Fault)
extern "x86-interrupt" fn general_protection_fault(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        GENERAL_PROTECTION_FAULT,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


/// [Page Fault](https://en.wikipedia.org/wiki/Page_fault),
/// [Exceptions: Page Fault](https://wiki.osdev.org/Exception#Page_Fault)
extern "x86-interrupt" fn page_fault(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        PAGE_FAULT,
        Info::PageFault {
            address: Cr2::read().into(),
            code: PageFaultInfo::from_bits_truncate(size::into_usize(error_code)),
        },
        Fatal::Yes,
    );
}


/// [Exceptions: x87 Floating-Point Exception](https://wiki.osdev.org/Exception#x87_Floating-Point_Exception)
extern "x86-interrupt" fn x87_floating_point(mut context: InterruptContext) {
    generic_trap(&mut context, X87_FLOATING_POINT, Info::None, Fatal::Yes);
}


/// [Exceptions: Alignment Check](https://wiki.osdev.org/Exception#Alignment_Check)
extern "x86-interrupt" fn alignment_check(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        ALIGNMENT_CHECK,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


/// [Machine-check exception](https://en.wikipedia.org/wiki/Machine-check_exception),
/// [Exceptions: Machine Check](https://wiki.osdev.org/Exception#Machine_Check)
extern "x86-interrupt" fn machine_check(mut context: InterruptContext) {
    generic_trap(&mut context, MACHINE_CHECK, Info::None, Fatal::Yes);
}


/// [Exceptions: SIMD Floating-Point Exception](https://wiki.osdev.org/Exception#SIMD_Floating-Point_Exception)
extern "x86-interrupt" fn simd_floating_point(mut context: InterruptContext) {
    generic_trap(&mut context, SIMD_FLOATING_POINT, Info::None, Fatal::Yes);
}


/// [Exceptions: Security Exception](https://wiki.osdev.org/Exception#Security_Exception)
extern "x86-interrupt" fn virtualization(mut context: InterruptContext) {
    generic_trap(&mut context, VIRTUALIZATION, Info::None, Fatal::Yes);
}


/// [Exceptions: Security Exception](https://wiki.osdev.org/Exception#Security_Exception)
extern "x86-interrupt" fn security_exception(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        SECURITY_EXCEPTION,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


/// Выполняет общую часть обработки для всех прерываний
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259).
/// Аргумент `number` задаёт номер прерывания в общей нумерации таблицы обработчиков прерываний
/// ([Interrupt descriptor table](https://en.wikipedia.org/wiki/Interrupt_descriptor_table), IDT).
fn generic_pic_interrupt(number: usize) {
    INTERRUPT_STATS[number].inc();
    unsafe {
        pic8259::end_of_interrupt(number - PIC_BASE);
    }
}


/// Обработчик прерывания таймера [Intel 8253/8254](https://en.wikipedia.org/wiki/Intel_8253)
/// ([programmable interval timer, PIT](https://en.wikipedia.org/wiki/Programmable_interval_timer)).
extern "x86-interrupt" fn timer(_context: InterruptContext) {
    pit8254::interrupt();
    generic_pic_interrupt(TIMER);
}


/// Обработчик прерывания клавиатуры.
extern "x86-interrupt" fn keyboard(_context: InterruptContext) {
    generic_pic_interrupt(KEYBOARD);
}


/// Обработчик каскадного прерывания первого контроллера
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259),
/// к которому подключён второй такой же.
extern "x86-interrupt" fn cascade(_context: InterruptContext) {
    generic_pic_interrupt(CASCADE);
}


/// Обработчик прерывания
/// [последовательных портов](https://en.wikipedia.org/wiki/Serial_port) номер 2 и 4.
extern "x86-interrupt" fn com2(_context: InterruptContext) {
    generic_pic_interrupt(COM2);
}


/// Обработчик прерывания
/// [последовательных портов](https://en.wikipedia.org/wiki/Serial_port) номер 1 и 3.
extern "x86-interrupt" fn com1(_context: InterruptContext) {
    generic_pic_interrupt(COM1);
}


/// Обработчик прерывания второго параллельного порта
/// ([Parallel port](https://en.wikipedia.org/wiki/Parallel_port)).
/// Так как через параллельные порты чаще всего подключались принтеры
/// ([Line printer](https://en.wikipedia.org/wiki/Line_printer)),
/// сохранилось их сокращение LPT.
extern "x86-interrupt" fn lpt2(_context: InterruptContext) {
    generic_pic_interrupt(LPT2);
}


/// Обработчик прерывания контроллера [дискет](https://en.wikipedia.org/wiki/Floppy_disk).
extern "x86-interrupt" fn floppy_disk(_context: InterruptContext) {
    generic_pic_interrupt(FLOPPY_DISK);
}


/// Обработчик прерывания первого и третьего параллельного порта
/// ([Parallel port](https://en.wikipedia.org/wiki/Parallel_port)).
/// Так как через параллельные порты чаще всего подключались принтеры
/// ([Line printer](https://en.wikipedia.org/wiki/Line_printer)),
/// сохранилось их сокращение LPT.
extern "x86-interrupt" fn lpt1(_context: InterruptContext) {
    generic_pic_interrupt(LPT1);
}


/// Обработчик прерываний
/// [часов реального времени (Real-time clock, RTC)](https://en.wikipedia.org/wiki/Real-time_clock).
extern "x86-interrupt" fn rtc(_context: InterruptContext) {
    rtc::interrupt();
    generic_pic_interrupt(RTC);
}


/// Обработчик прерывания незарезервированного входа `0x9` каскадной пары
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259).
extern "x86-interrupt" fn free_29(_context: InterruptContext) {
    generic_pic_interrupt(FREE_29);
}


/// Обработчик прерывания незарезервированного входа `0xA` каскадной пары
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259).
extern "x86-interrupt" fn free_2a(_context: InterruptContext) {
    generic_pic_interrupt(FREE_2A);
}


/// Обработчик прерывания незарезервированного входа `0xB` каскадной пары
/// [PIC 8259](https://en.wikipedia.org/wiki/Intel_8259).
extern "x86-interrupt" fn free_2b(_context: InterruptContext) {
    generic_pic_interrupt(FREE_2B);
}


/// Обработчик прерывания мыши.
extern "x86-interrupt" fn ps2_mouse(_context: InterruptContext) {
    generic_pic_interrupt(PS2_MOUSE);
}


/// Обработчик прерывания сопроцессора.
extern "x86-interrupt" fn coprocessor(_context: InterruptContext) {
    generic_pic_interrupt(COPROCESSOR);
}


/// Обработчик прерывания первого контроллера
/// [PATA](https://en.wikipedia.org/wiki/Parallel_ATA).
extern "x86-interrupt" fn ata0(_context: InterruptContext) {
    generic_pic_interrupt(ATA0);
}


/// Обработчик прерывания второго контроллера
/// [PATA](https://en.wikipedia.org/wiki/Parallel_ATA).
extern "x86-interrupt" fn ata1(_context: InterruptContext) {
    generic_pic_interrupt(ATA1);
}


/// Выполняет общую часть обработки для всех прерываний
/// [APIC](https://en.wikipedia.org/wiki/Advanced_Programmable_Interrupt_Controller#APIC_timer).
/// Аргумент `number` задаёт номер прерывания в общей нумерации таблицы обработчиков прерываний
/// ([Interrupt descriptor table](https://en.wikipedia.org/wiki/Interrupt_descriptor_table), IDT).
fn generic_apic_interrupt(number: usize) {
    INTERRUPT_STATS[number].inc();
    LocalApic::end_of_interrupt();
}


/// Обработчик прерывания
/// [таймера APIC](https://en.wikipedia.org/wiki/Advanced_Programmable_Interrupt_Controller#APIC_timer).
extern "x86-interrupt" fn apic_timer(mut context: InterruptContext) {
    Process::preempt(&mut context);
    generic_apic_interrupt(APIC_TIMER);
}


/// Обработчик ложных прерываний
/// ([spurious interrupt](https://en.wikipedia.org/wiki/Interrupt#Spurious_interrupts))
/// [APIC](https://en.wikipedia.org/wiki/Advanced_Programmable_Interrupt_Controller).
extern "x86-interrupt" fn apic_spurious(_context: InterruptContext) {
    generic_apic_interrupt(APIC_SPURIOUS);
}


#[doc(hidden)]
pub mod test_scaffolding {
    use super::{Idt, IdtEntry, InterruptContext, COUNT, DEBUG};


    static mut IDT: Idt = Idt([IdtEntry::missing(); COUNT]);


    pub fn set_debug_handler(handler: extern "x86-interrupt" fn(InterruptContext)) {
        unsafe {
            IDT = Idt::new();
            IDT.0[DEBUG].set_handler(handler);
            IDT.load();
        }
    }
}
